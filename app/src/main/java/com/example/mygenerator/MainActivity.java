package com.example.mygenerator;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.annotation.Test02Annotation;


public class MainActivity extends AppCompatActivity {

    @Test02Annotation(branch = "brancha")
    String mBranch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initData();
    }

    private void initData() {
        //        Test02 test02 = new Test02();
        //        test02.run();
        Log.d("MainActivity", "mBranch:" + mBranch);
    }
}
