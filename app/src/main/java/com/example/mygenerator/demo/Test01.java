package com.example.mygenerator.demo;

import java.lang.reflect.Field;

/**
 * Created by MajinBuu on 2020/1/13 0013.
 *
 * @desc ${todo}.
 */

public class Test01 {

    @Test01Annotation(msg = "zhang_san")
    private String mName;

    public static void main(String[] args) {
        Field name = null;
        try {
            name = Test01.class.getDeclaredField("mName");
            name.setAccessible(true);
            Test01Annotation annotation = name.getAnnotation(Test01Annotation.class);
            if (annotation != null) {
                System.out.println("msg:" + annotation.msg());
            } else {
                System.out.println("annotation == null");
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }
}
