package com.example.mygenerator.demo;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by MajinBuu on 2020/1/13 0013.
 *
 * @desc ${todo}.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Test01Annotation {
    String msg() default "default_value";

}
