package com.example.complier;

import com.example.annotation.Test02Annotation;
import com.google.auto.service.AutoService;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Completion;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;

/**
 * Created by MajinBuu on 2020/1/14 0014.
 *
 * @desc ${todo}.
 */
@AutoService(Processor.class)
public class TestComplier extends AbstractProcessor {

    protected TestComplier() {
        super();
    }

    @Override
    public Set<String> getSupportedOptions() {
        return super.getSupportedOptions();
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }

    @Override
    public synchronized void init(ProcessingEnvironment processingEnvironment) {
        super.init(processingEnvironment);
    }

    @Override
    public Iterable<? extends Completion> getCompletions(Element element, AnnotationMirror annotationMirror, ExecutableElement executableElement, String s) {
        return super.getCompletions(element, annotationMirror, executableElement, s);
    }

    @Override
    protected synchronized boolean isInitialized() {
        return super.isInitialized();
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        LinkedHashSet<String> set = new LinkedHashSet<>();
        set.add(Test02Annotation.class.getCanonicalName());
        return set;
    }

    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {
        JavaPoetTest.demo();
        //        findAndParseTargets(roundEnvironment);
        return true;
    }

    private Map findAndParseTargets(RoundEnvironment env) {
        //get all Test02Annotation elements
        Set<? extends Element> elements = env.getElementsAnnotatedWith(Test02Annotation.class);
        System.out.println("elements:" + elements);
        for (Element element : elements) {

        }
        //make a java file

        return null;
    }
}
