package com.example.complier;

import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;

import java.io.File;
import java.io.IOException;

import javax.lang.model.element.Modifier;

public class JavaPoetTest {

    public static void main(String[] args) {
        demo();
    }

    public static void demo() {
        MethodSpec main = MethodSpec.methodBuilder("main")
                .addModifiers(Modifier.PUBLIC,Modifier.STATIC)
                .returns(TypeName.VOID)
                .addParameter(String[].class,"args")
                .addStatement("$T.out.println($S)",System.class,"Hello World!")
                .build();

        TypeSpec typeSpec = TypeSpec.classBuilder("HelloWord")
                .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                .addMethod(main)
                .build();

        JavaFile javaFile = JavaFile.builder("com.example.mygenerator", typeSpec).build();

        try {
            javaFile.writeTo(System.out);
            javaFile.writeTo(new File("app/src/main/java"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
